let points = +prompt('Введите число пунктов', '');
const listArr = [];

for (let i = 0; i < points; i++) {
  listArr [i] = prompt('Введите содержимое пункта', '');
};
const createList = listArr.map(function(content){
  let li = document.createElement('li');
  li.innerHTML = `${content}`;
  return li;
});
const ul = document.createElement('ul');
for(let key of createList){
  ul.appendChild(key);
};
document.body.appendChild(ul);


let timer = document.createElement('div');
let t = 10
timer.innerText = t
document.body.appendChild(timer);
let timerCountdown = setInterval(function(){
  timer.innerText = --t;
},1000);

setTimeout(function(){
  document.body.removeChild(ul);
  clearInterval(timerCountdown);
  document.body.removeChild(timer);
},10000);
